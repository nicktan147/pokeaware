<?php
session_start();
date_default_timezone_set('Asia/Singapore');

/******* Configuration ********/

$down = false; // Maintenance?
$file = 'banlist.txt'; //banlist ip
$mysqli = new mysqli("localhost", "root", "151505", "pokemondb");
/* ************************* */

$now = date("Y-m-d H:i:s");
$clientip = $_SERVER['REMOTE_ADDR'];

// Maintenance Section
if ($down == true) {
include 'maintenance.php';
exit;
}

// MYSQL connection check
if (mysqli_connect_errno())
		{
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
		}

// Banlist IP Checking Section

$current = file_get_contents($file);
if (strpos($current,$clientip)) {
$newURL = "http://google.com";
header('Location: '.$newURL);
exit;
}

// Initialization

if (!isset($_SESSION['wrongy'])) {
$_SESSION['wrongy'] = 0;
}

if ($_SESSION['wrongy'] > 1) {
	
$url = "https://freegeoip.net/json/".$clientip;

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,$url);
$result=curl_exec($ch);
curl_close($ch);

$clientdata = json_decode($result, true);

unset($current);

$current .= $_SERVER['REMOTE_ADDR']." - ".$clientdata['country_code']."\n";
file_put_contents($file, $current);

$newURL = "https://google.com";
header('Location: '.$newURL);
exit;
}
