**PokeAware!**

Pokeaware is live visualization of all the pokemon (with option to show gyms and pokestops) in your area. This is a proof of concept that we can load all the pokemon visible nearby given a location. Currently runs on a Flask server displaying Google Maps with markers on it. Include with login system with and geo location check.

**Main page**

![main.jpg](https://bitbucket.org/repo/LGLbek/images/4250551612-main.jpg)

**After Login**

![afterlogin.jpg](https://bitbucket.org/repo/LGLbek/images/274446465-afterlogin.jpg)

**Scanner**

![Capture.JPG](https://bitbucket.org/repo/LGLbek/images/575305856-Capture.JPG)