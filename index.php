<?php

include 'config.php';
$errorauth = "none"; // Display error message

if (isset($_POST['passwd']) && isset($_POST['userid'])) {

$username = $_POST['userid'];
$username = preg_replace("/[^a-zA-Z0-9]+/", "", $username);
$password = $_POST['passwd'];
$password = preg_replace("/[^a-zA-Z0-9]+/", "", $password);
$user_data = array();


	$query = "SELECT Status,Node1,Node2,Node3,Node4,Node5,loggedin FROM Users WHERE Username = ? && Password = ?";
	if ($stmt = $mysqli->prepare($query))
		{
		$stmt->bind_param("ss", $username, $password);
		$stmt->execute();
		$stmt->bind_result($user_status, $user_node1, $user_node2, $user_node3, $user_node4, $user_node5, $user_loggedin);
		if ($stmt)
			{
			while ($stmt->fetch())
				{
				$user_data['user_status'] = $user_status;
				$user_data['user_node1'] = $user_node1;
				$user_data['user_node2'] = $user_node2;
				$user_data['user_node3'] = $user_node3;
				$user_data['user_node4'] = $user_node4;
				$user_data['user_node5'] = $user_node5;
				}
			}

		$stmt->close();
		}
	
	if (!empty($user_data))
		{
	$query = "SELECT Username,Map,UseDateTime FROM lastUse"; 
	
	if ($stmt = $mysqli->prepare($query))
		{
		$stmt->execute();
		$stmt->bind_result($user_lastuse_name, $user_lastuse_map, $user_lastuse_time);
		if ($stmt)
			{
				$x = 1;
			while ($stmt->fetch())
				{
				$user_data['last_user_map']['name'][$x] = $user_lastuse_name;
				$user_data['last_user_map']['map'][$x] = $user_lastuse_map;
				$user_data['last_user_map']['time'][$x] = $user_lastuse_time;
				$x++;
				}
			}

		$stmt->close();
		}
		}
	
	//if (!empty($user_data) || $user_data['user_status'] == 'I') {
	if (!empty($user_data)){
	$options = [
    'cost' => 12,
	];
	
	//Check User Accessible Map
	$content = "";
	
	for ($nodesnum = 1; $nodesnum < 6; $nodesnum++) {
		
		if ($user_data['user_node'.$nodesnum] == "A" ) {
			if ($nodesnum == 6) {
			$content .= '<div class=:col-md-6"><img width="80" class="image-node" height="80" src="pokemon-icon.png"><a class="text-node" href="#">Coming Soon</a><br><br><div>';	
			}
			else {
			$to_time = strtotime($now);
			$from_time = strtotime($user_data['last_user_map']['time'][$nodesnum]);
			$interval  = abs($datetime2 - $datetime1);
			$timeused = round(abs($to_time - $from_time) / 60);
			
			$hours = floor($timeused / 60);
			$min = $timeused - ($hours * 60);
			$data = $hours." Hours ".$min. " Minutes ago";

			$url = 'https://pokeaware.com:8'.$nodesnum;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
			curl_setopt($ch, CURLOPT_NOBODY, true);    // we don't need body
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT,10);
			$output = curl_exec($ch);
			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);

			if ($httpcode != "200" || $httpcode != 200) {
			$offline_node = true;

			}
			
			if ($offline_node == true) {
			$content .= '<div class=:col-md-6"><img width="80" class="image-node" height="80" src="pokemon-icon-offline.png"><a class="text-node" href="#">Scanner '.$nodesnum.' (Offline)';
			}
			else {
			$content .= '<div class=:col-md-6"><img width="80" class="image-node" height="80" src="pokemon-icon.png"><a class="text-node" href="datacheck.php?map='.$nodesnum.'&idsession='.$hashed_md5.'&username='.$username.'">Scanner '.$nodesnum; 
			}

			$content .= '<span class="right" title="tipso" data-tipso-title="Last used by:" data-tipso="'.$user_data['last_user_map']['name'][$nodesnum]. ' - ' .$data.'"> <img src="pokeball.png" width="30" height="30"></span></a><br><br><div>';
			unset ($offline_node);
			//echo "<script>console.log('".$user_data['last_user_map']['time'][$nodesnum]."'); console.log('".$data."');                        </script>";
			
			}
		}
	}
	
	$hash = password_hash($password, PASSWORD_BCRYPT, $options);
	$hashed_md5 = md5($hash);
	
	$query = "UPDATE Users SET HashPass=?,updateTime=? WHERE Username = ? && Password = ?";
	
	if ($stmt = $mysqli->prepare($query))
		{
		$stmt->bind_param("ssss", $hashed_md5, $now, $username, $password);
		$stmt->execute();
		
		if ($stmt->errno) {
			echo "FAILURE!!! " . $stmt->error;
			exit;
		}
		
		$stmt->close();
		}
	
	$mysqli->close();
	$_SESSION['wrongy'] = 0;
	
    setcookie("logged", $hash, time()+3600);
	$showlogin = "display:none;";
	}

else {
    $_SESSION['wrongy'] = $_SESSION['wrongy'] + 1;	
	$errorauth = "block";
	$mysqli->close();
}
}
$mysqli->close();
?>


<meta name="viewport" content="user-scalable=no"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="css/tipso.css">
<script src="js/tipso.min.js"></script>

<script>
jQuery(document).ready(function(){
jQuery('.right').tipso({
				position: 'right',
				background: 'rgba(0,0,0,0.8)',
				titleBackground: 'tomato',
				useTitle: false,
			});
jQuery('.right').tipso('show');
});
</script>			

<style>
 .css-input { border-color:#cccccc; box-shadow: 0px 0px 5px 0px rgba(42,42,42,.75); border-style:dashed; border-width:0px; border-radius:7px; font-size:20px; padding:13px; text-align:left;  } 
		 .css-input:focus { outline:none; } 

.btn {
  background: #f5c573;
  background-image: -webkit-linear-gradient(top, #f5c573, #d99e09);
  background-image: -moz-linear-gradient(top, #f5c573, #d99e09);
  background-image: -ms-linear-gradient(top, #f5c573, #d99e09);
  background-image: -o-linear-gradient(top, #f5c573, #d99e09);
  background-image: linear-gradient(to bottom, #f5c573, #d99e09);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
  margin-left: 55px;
}

.btn:hover {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}

.btn2 {
  margin-left: 65px;
}

.login {
    width: 300px;
    height: 600px;
    position: absolute;
    top:0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
}

body {
  font: 13px/20px "Lucida Grande", Tahoma, Verdana, sans-serif;
  color: #404040;
background-image:url(bg.jpg);
    background-repeat:no-repeat;
-webkit-background-size:cover;
-moz-background-size:cover;
-o-background-size:cover;
background-size:cover;
background-position:center;
}

html {
    height: 100%
}
a:hover, a:visited, a:link, a:active
{
    text-decoration: none;
	color:white;
}

.text-node {
  z-index: 100;
  position: absolute;
  color: white;
  font-size: 24px;
  font-weight: bold;
  margin-top:25px;
  margin-left:40px;
}
.text-node1 {
  z-index: 100;
  position: absolute;
  color: white;
  font-size: 24px;
  font-weight: bold;
  left: 150px;
  top: 30px;
}
.text-node2 {
  z-index: 100;
  position: absolute;
  color: white;
  font-size: 24px;
  font-weight: bold;
  left: 150px;
  top: 160px;
}
.text-node3 {
  z-index: 100;
  position: absolute;
  color: white;
  font-size: 24px;
  font-weight: bold;
  left: 150px;
  top: 290px;
}

a {text-decoration: none}
</style>

<div class="container">
  <div class="login">
	<?php if ($content !== "") { 
		echo $content;
		}
	?>
	<title>Pokeaware</title>
    <form action="index.php" method="post" style="<?php echo $showlogin; ?>">
<img src="http://i1279.photobucket.com/albums/y523/textcraft/Sep%202016%20-%201/d57862c7667c8e65f117ead5d415fd88930071a4da39a3ee5e6b4b0d3255bfef95601890afd80709da39a3ee5e6b4b0d3255bfef95601890afd80709777f_zpsx0xoq7c0.png" alt="Pokemon Font" style="margin-left: -180px;">
      <p><div style="width:250px;height:30px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;background:rgba(255,110,94,0.6);-webkit-box-shadow: #B3B3B3 14px 14px 14px;-moz-box-shadow: #B3B3B3 14px 14px 14px; box-shadow: #B3B3B3 14px 14px 14px;display:<?php echo $errorauth; ?>;" >
	  <span style="margin-left: 30px;"><strong>Opps wrong details given :(</strong></span>
	  </div></p>
	  <p><input type="text" class="css-input" name="userid" placeholder="Trainer Name"></p>
	  <p><input type="password" class="css-input" name="passwd" placeholder="Password please :)"></p>

	<input type="image" class="btn2" src="pokeball.png" width="80" height="80"/>
    </form>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54568750-4', 'auto');
  ga('send', 'pageview');

</script>
